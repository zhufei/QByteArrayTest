{-# LANGUAGE ScopedTypeVariables #-}

import      qualified Graphics.UI.Qtah.Core.QByteArray as QByteArray
import      Foreign.Hoppy.Runtime
import      Foreign.C
import      Test.Hspec
hello :: IO CString
hello = newCString "hello"

world :: IO CString
world = newCString "world"

main :: IO ()
main = hspec $ do
  describe "QByteArray" $ do
    describe "aSSIGN" $ do
      it "assign method" $ do
        sw <- world
        byteArrayPtrWorld :: QByteArray.QByteArray <- QByteArray.newFromData  sw

        sh <- hello
        byteArrayPtrhello :: QByteArray.QByteArray <- QByteArray.newFromData  sh

        byteArrayPtrhw <- QByteArray.aSSIGN byteArrayPtrhello byteArrayPtrWorld

        cs <- QByteArray.getData byteArrayPtrhw
        strHaskell <- peekCString cs        
        strHaskell `shouldBe` "world"