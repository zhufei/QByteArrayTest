{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import      qualified Graphics.UI.Qtah.Core.QByteArray as QByteArray
import      Foreign.Hoppy.Runtime
-- import      qualified Prelude as QtahP
import      qualified Foreign as HoppyF
import      Foreign.C
-- import      Graphics.UI.Qtah.Generated.Core.QByteArray

hello :: IO CString
hello = newCString "hello"

world :: IO CString
world = newCString "world"

main :: IO ()
main = do
  -- byteArrayPtr ::  QByteArray.QByteArray  <- QByteArray.new 
  
  sw <- world
  byteArrayPtrWorld :: QByteArray.QByteArray <- QByteArray.newFromData  sw




  sh <- hello
  byteArrayPtrhello :: QByteArray.QByteArray <- QByteArray.newFromData  sh




  byteArrayPtrhw <- QByteArray.aSSIGN byteArrayPtrhello byteArrayPtrWorld
  
  

  cs <- QByteArray.getData byteArrayPtrhw
  strHaskell <- peekCString cs

  putStrLn strHaskell
