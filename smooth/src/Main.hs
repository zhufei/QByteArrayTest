module Main where



-- import    Graphics.UI.Qtah.Generator.Interface.Imports
-- import    Graphics.UI.Qtah.Generator.Types

-- import    Graphics.UI.Qtah.Generator.Common
-- import    Graphics.UI.Qtah.Generator.Flags


-- import    System.Environment
-- import    Graphics.UI.Qtah.Generator.Interface.Core.QByteArray


import System.Exit (exitFailure)
import System.Environment (getArgs)
import Data.List (intercalate)




import    Graphics.UI.Qtah.Generator.Module 
import    Graphics.UI.Qtah.Generator.Interface.Core as Core


import Foreign.Hoppy.Generator.Spec (
  Interface,
  Module,
  interface,
  interfaceAddHaskellModuleBase,
  moduleModify',
  moduleSetCppPath,
  moduleSetHppPath,
  )

import Foreign.Hoppy.Generator.Main (run)
import Foreign.Hoppy.Generator.Std

modules :: [AModule]
modules =
  concat
  [ [ 
    AHoppyModule mod_std
    ]
  , Core.modules
  -- , Gui.modules
  -- , Internal.modules
  -- , Widgets.modules
  ]



interfaceResult :: Either String Interface
interfaceResult =
  interfaceAddHaskellModuleBase ["Graphics", "UI", "Qtah"] =<<
  interface "qtah" (concatMap aModuleHoppyModules Main.modules)


main :: IO ()
main = do
  -- maybeStr <- lookupEnv "QtMsBuild"

  -- case maybeStr of 
  --   Just str ->
  --     putStrLn str
  --   Nothing ->
  --     putStrLn "not found QTAH_QT"

  -- putStrLn qmakeExecutable 

  case interfaceResult of
    Left errorMsg -> do
      putStrLn $ "Error initializing interface: " ++ errorMsg
      exitFailure
    Right iface -> do
      args <- getArgs
      case args of
        -- ["--qt-version"] -> putStrLn $ intercalate "." $ map show qtVersion
        -- ["--qmake-executable"] -> putStr $ unlines $ qmakeExecutable : qmakeArguments
        _ -> do
          _ <- run [iface] args
          return ()

  putStrLn "hello world"
  
