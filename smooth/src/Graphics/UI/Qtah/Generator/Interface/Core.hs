module Graphics.UI.Qtah.Generator.Interface.Core (modules) where


import qualified Graphics.UI.Qtah.Generator.Interface.Core.QByteArray as QByteArray
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QString as QString
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QChar as QChar

import Graphics.UI.Qtah.Generator.Module (AModule)


{-# ANN module "HLint: ignore Use camelCase" #-}




modules :: [AModule]
modules =
  concat
  [ [
    QByteArray.aModule
    , QChar.aModule
    , QString.aModule
    ]
  ]















